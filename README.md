# [Rock-Paper-Scissor](http://en.wikipedia.org/wiki/Rock-paper-scissors)

- Game
  - [x] Can I play Player vs Computer?
  - [x] Can I play Computer vs Computer?
  - [] Can I play a different game each time?
  
- Scoope of Improvement
  - [ ] Very Minimal UI. [known Issue: User Input doesn't display on the console when typing]
  - [ ] Game/AI Logic.
  - [ ] Code Coverage.
    - [x] Rules (Game logic)
    - [ ] Game, GameMode, Players.
  
## Environment

- Java `1.8.0_242` or higher
- Sbt version `1.3.2`
- Scala version `2.12.10`
- Container - `Docker`

### Librabry

- Codeformat -> `Scalafmt 2.2.0`
- Linting & Fix -> `Scalafx 0.9.11`
- Git within build tool -> `sbt-git 1.0.0`
- Dependency Update -> `sbt-updates" 0.5.0`
- Testing -> `scalatest 3.1.1` & `scalacheck 1.14.3`
- Code Coverage -> `sbt-scoverage 1.6.0`

### Execution in Local

- Clone the repo & go to the directory
- Enter to the Game
  - `sbt run`
  - Follow then the instruction
- Run Test & Check TestReport
  - `sbt test` or `sbt coverage`
  - Check `${repo}/src/target/test-reports/index.html` [may not find html file, bug for parsing]
- Run Coverage & Check
  - `sbt coverageReport`
  - Check `${repo}/src/target/scala-2.12/scoverage-report/index.html`
- Format Code
  - `sbt scalafmt`
  - `sbt test:scalafmt`
- Fix Code
  - `sbt scalafix`
- Generate Documentation
  - `scaladoc src/main/scala/com/play/*`

### Project Key Shortcut

```console
───────────────────────────
  List of defined aliases
────────┬──────────────────
cd      │ project
root    │ cd play
c       │ compile
ca      │ compile all
t       │ test
r       │ run
up2date │ dependencyUpdates
────────┴──────────────────
```
  
## Using Docker Container

### Build Docker Image (Mandatory as Image is not push to the registry)
  
- `docker build -t IMAGE_NAME:TAG -f path/to/Dockerfile CONTEXT_NAME(Optional)`
- example: `docker build -t rps:v1 -f $(pwd)/rock-paper-scissor/Dockerfile rock-paper-scissor`

### Create Container from the Image & Enter to the container directly

- `docker run -it --name game_1 -v $(pwd)/libDoc:/sharedDoc rps:v1`
- Start game
  - `sbt run`
  - Check test result in shared directory in the host
  - `sbt coverage`
  - Check Test Report in Host `${pwd}/src/target/test-reports/index.html`
  - `sbt coverageReport`
  - Check Coverage Report in Host `${pwd}/src/target/scala-2.12/scoverage-report/index.html`
- Generate Documentation in container *sharedDoc* & access from host directory named *libDoc*
  - `scaladoc src/main/scala/com/play/* -d /sharedDoc`
- Or Create Container from the Image & Enter to the container & Start the game directly
  - `docker run -it --name game_1 -v $(pwd)/libDoc:/sharedDoc rps:v1 sbt run`
- Delete Containers
  - `docker rm $(docker ps -aq)`

### Using Docker-compose

- `docker-compose run game`
- Start game
  - `sbt run`
- Check test result in shared directory in the host
  - `sbt clean coverage test`
- Check Test Report in Host `${pwd}/src/target/test-reports/index.html`
  - `sbt coverageReport`
- Check Coverage Report in Host `${pwd}/src/target/scala-2.12/scoverage-report/index.html`
- Delete Containers
  - `docker-compose down`
  - `docker rm $(docker ps -aq)`

### Example of the Game Console

```console
Please choose the game mode you want to play:
1) Human Vs Computer
2) Computer Vs Computer
>
1
Enter Number of times Computers'll play together
5

Start the Game
Rock-Paper-Scissor

Pick a Sign: R/Rock, P/Paper or S/Scissor
=========================================
Rock
Computer Won , Player Lost
=========================
Pick a Sign: R/Rock, P/Paper or S/Scissor
=========================================
P
Computer Won , Player Lost
=========================
Pick a Sign: R/Rock, P/Paper or S/Scissor
=========================================
Paper
Computer Won , Player Lost
=========================
Pick a Sign: R/Rock, P/Paper or S/Scissor
=========================================
R
Player Won , Computer Lost
=========================
Pick a Sign: R/Rock, P/Paper or S/Scissor
=========================================
scissor
Computer Won , Player Lost
=========================
See you next time

*Or 2nd Option for Computer vs Computer*

Please choose the game mode you want to play:
1) Human Vs Computer
2) Computer Vs Computer
>
2
Enter Number of times Computers'll play together
10

Start the Game
Rock-Paper-Scissor

Computer-2 Won , Computer-1 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
Match Draw
==============================
Match Draw
==============================
Computer-1 Won , Computer-2 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
Computer-2 Won , Computer-1 Lost
==============================
See you next time

```
