import Dependencies._

ThisBuild / organization := "com"
ThisBuild / scalaVersion := "2.12.10"
ThisBuild / version := "0.0.1-SNAPSHOT"

ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-language:_",
  "-unchecked",
  // "-Wunused:_",
  "-Xfatal-warnings",
  // "-Ymacro-annotations",
  "-Ywarn-unused",
  // "-Ywarn-unused-import",
  "-Yrangepos"
  // "-Yno-adapted-args"
  // "-Ywarn-adapter-args"
)

lazy val `play` =
  project
    .in(file("."))
    .settings(
      name := "play",
      addCompilerPlugin(org.typelevel.`kind-projector`),
      addCompilerPlugin(scalafixSemanticdb),
      libraryDependencies ++= Seq(
        // main dependencies
      ),
      libraryDependencies ++= Seq(
        com.github.alexarchambault.`scalacheck-shapeless_1.14`,
        org.scalacheck.scalacheck,
        org.scalatest.scalatest,
        org.scalactic.scalactic,
        org.scalatestplus.`scalatestplus-scalacheck`,
        org.pegdown.pegdown
      ).map(_ % Test),
      Compile / console / scalacOptions --= Seq(
        "-Wunused:_",
        "-Xfatal-warnings"
      ),
      Test / console / scalacOptions :=
        (Compile / console / scalacOptions).value
    )
