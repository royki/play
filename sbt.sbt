import Util._

Global / onChangedBuildSource := ReloadOnSourceChanges

Test / parallelExecution := false
// Test / testOptions += Tests.Argument(TestFrameworks.ScalaTest, "-oSD")

This / testOptions ++= Seq(
  Tests.Argument(TestFrameworks.ScalaTest, "-oSD"),
  // Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports"),
  Tests.Argument(TestFrameworks.ScalaCheck, "-s", "10")
)

Test / turbo := true

ThisBuild / autoStartServer := false
ThisBuild / includePluginResolvers := true
ThisBuild / turbo := true
ThisBuild / useSuperShell := false

ThisBuild / watchBeforeCommand := Watch.clearScreen
ThisBuild / watchTriggeredMessage := Watch.clearScreenOnTrigger
ThisBuild / watchForceTriggerOnAnyChange := true

ThisBuild / shellPrompt := { state =>
  s"${prompt(projectName(state))}> "
}

ThisBuild / watchStartMessage := {
  case (iteration, ProjectRef(build, projectName), commands) =>
    Some {
      s"""|~${commands.map(styled).mkString(";")}
          |Monitoring source files for ${prompt(projectName)}...""".stripMargin
    }
}
