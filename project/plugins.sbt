ThisBuild / scalaVersion := "2.12.10"
ThisBuild / useSuperShell := false
ThisBuild / autoStartServer := false

// plugin of git command line features directly inside sbt
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")

// sbt plugin to format code
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.2.0")

// Refactoring and linting tool for Scala
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.11")

// sbt plugin checks Maven and Ivy repositories for dependency update
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.0")

// test coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")
